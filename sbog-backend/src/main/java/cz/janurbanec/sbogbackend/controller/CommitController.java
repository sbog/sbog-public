package cz.janurbanec.sbogbackend.controller;

import cz.janurbanec.sbogbackend.model.Comment;
import cz.janurbanec.sbogbackend.model.Commit;
import cz.janurbanec.sbogbackend.model.ExtraCommit;
import cz.janurbanec.sbogbackend.model.Project;
import cz.janurbanec.sbogbackend.repository.CommentRepository;
import cz.janurbanec.sbogbackend.repository.ExtraCommitRepository;
import cz.janurbanec.sbogbackend.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class CommitController {
    @Autowired
    private ExtraCommitRepository extraCommitRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private CommentRepository commentRepository;

    @PostMapping("/api/extraCommit/add")
    public ResponseEntity<ExtraCommit> addExtraCommit(@RequestBody ExtraCommit extraCommit) {
            extraCommitRepository.save(extraCommit);

            return new ResponseEntity<>(extraCommit, HttpStatus.CREATED);
    }

    @DeleteMapping("/api/extraCommit/delete")
    public ResponseEntity<Void> deleteExtraCommit(@RequestParam(name = "extraCommitId") UUID extraCommitId) {
        if (extraCommitRepository.existsById(extraCommitId)) {
            extraCommitRepository.deleteById(extraCommitId);

            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/api/commit/getCommits")
    public ResponseEntity<List<Commit>> getCommits(
            @RequestParam(name = "projectId") UUID projectId,
            @RequestParam(name = "branchName") String branchName
    ) {
        if (projectRepository.existsById(projectId)) {
            Project project = projectRepository.getById(projectId);

            return new ResponseEntity<>(
                    project.getCommits(branchName),
                    HttpStatus.OK
            );
        }

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/api/extraCommit/getComments")
    public ResponseEntity<List<Comment>> getComments(@RequestParam(name = "extraCommitId") UUID extraCommitId) {
        if (extraCommitRepository.existsById(extraCommitId)) {
            ExtraCommit extraCommit = extraCommitRepository.getById(extraCommitId);

            return new ResponseEntity<>(
                    extraCommit.getComments(),
                    HttpStatus.OK
            );
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping("/api/extraCommit/addComment")
    public ResponseEntity<String> addComments(@RequestBody Comment comment) {
        comment.setDate(new Date());
        commentRepository.save(comment);

        return new ResponseEntity<>(
                "added",
                HttpStatus.OK
        );
    }
}
