package cz.janurbanec.sbogbackend.controller;

import cz.janurbanec.sbogbackend.git.RepositoryManager;
import cz.janurbanec.sbogbackend.model.User;
import cz.janurbanec.sbogbackend.repository.UserRepository;
import cz.janurbanec.sbogbackend.service.AddUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddUserService addUserService;

    @PostMapping("/api/addUser")
    public ResponseEntity<User> registration(@RequestBody User user) {
        return new ResponseEntity<>(user, HttpStatus.OK);
        /*if (userRepository.findByUsername(user.getUsername()) == null) {
            try {
                addUserService.saveUser(user);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } else throw new UserExistException(user.getUsername());*/
    }

    @GetMapping("/api/user/getUser")
    public HttpEntity<User> getUser() {
        return new ResponseEntity<>(userRepository.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        ), HttpStatus.OK);
    }

    @GetMapping("api/test")
    public List<String> test() {
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        return RepositoryManager.getRepositoriesByMember(user);
    }
}
