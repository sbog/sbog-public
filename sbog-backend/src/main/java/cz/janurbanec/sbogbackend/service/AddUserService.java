package cz.janurbanec.sbogbackend.service;


import cz.janurbanec.sbogbackend.model.User;

import java.io.IOException;

public interface AddUserService {
    void saveUser(User user) throws IOException, InterruptedException;
}
