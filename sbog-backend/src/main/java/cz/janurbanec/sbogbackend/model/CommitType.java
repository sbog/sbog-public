package cz.janurbanec.sbogbackend.model;

public enum CommitType {
    NO_STATUS,
    TO_CHECK,
    SUBMITTED
}
