package cz.janurbanec.sbogbackend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.stream.Stream;
import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    STUDENT("Student", "STUDENT"),
    TEACHER("Učitel", "TEACHER"),
    ADMIN("Administrátor", "ADMIN");

    private final String name;
    private final String code;

    Role(String name, String code) {
        this.name = name;
        this.code = code;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getAuthority() {
        return name;
    }

    @JsonCreator
    public static Role decode(final String code) {
        return Stream.of(Role.values()).filter(role -> role.getCode().equals(code)).findFirst().orElse(null);
    }
}
