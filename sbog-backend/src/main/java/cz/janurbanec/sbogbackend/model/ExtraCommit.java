package cz.janurbanec.sbogbackend.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "EXTRA_COMMITS")
public class ExtraCommit {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "ID_EXTRA_COMMIT", columnDefinition = "VARCHAR2(40)")
    private UUID id;

    @Basic
    @Column(name = "NAME_EXTRA_COMMIT", columnDefinition = "VARCHAR2(40)")
    private String name;

    @Basic
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "TYPE_EXTRA_COMMIT", columnDefinition = "NUMBER")
    private CommitType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PROJECT")
    @JsonIgnore
    private Project project;

    @OneToMany(mappedBy = "extraCommit")
    @JsonIgnore
    private List<Comment> comments;

    public ExtraCommit() {
    }

    public ExtraCommit(String name, CommitType type, Project project) {
        this.name = name;
        this.type = type;
        this.project = project;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommitType getType() {
        return type;
    }

    public void setType(CommitType type) {
        this.type = type;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
