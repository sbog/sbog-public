package cz.janurbanec.sbogbackend.git;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import nl.tudelft.ewi.gitolite.ManagedConfig;
import nl.tudelft.ewi.gitolite.ManagedConfigFactory;
import nl.tudelft.ewi.gitolite.objects.Identifier;
import nl.tudelft.ewi.gitolite.parser.rules.RepositoryRule;

public class RepositoryManager {

  private static ManagedConfig getManagedConfig() throws IOException, InterruptedException {
    return new ManagedConfigFactory()
        .repositoryFolder(new File(
            System.getProperty("user.home").substring(0, System.getProperty("user.home").lastIndexOf(System.getProperty("file.separator")) + 1)
                + "sbog"
                + System.getProperty("file.separator")
                + "gitolite-admin"))
        .gitManagerFactory(new SbogGitManagerFactory())
        .init("git@sbog-gitserver:gitolite-admin");
  }

  public static RepositoryRule getRepository(String name) {
    AtomicReference<RepositoryRule> repositoryRule = new AtomicReference<>(null);
    ManagedConfig managedConfig = null;
    try {
      managedConfig = getManagedConfig();
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }
    if (managedConfig != null) {
      managedConfig.readConfig(
          config -> repositoryRule.set(config.getFirstRepositoryRule(new Identifier(name)))
      );
    }
    return repositoryRule.get();
  }

  public static List<String> getRepositoryMembers(String repoName) {
    List<String> members = new ArrayList<>();

    RepositoryManager
        .getRepository(repoName)
        .getRules()
        .forEach(
            accessRule -> accessRule.getMembers().getMembersStream().forEach(
                identifier -> members.add(identifier.getPattern())
            )
        );

    return members;
  }

  public static List<String> getRepositoriesByMember(String memberName) {
    List<String> repositories = new ArrayList<>();

    try {
      getManagedConfig().readConfig(
          config -> config.getRules().stream().map(
              rule -> (RepositoryRule) rule).filter(
                  repositoryRule -> repositoryRule.getRules().stream().anyMatch(
                      accessRule -> accessRule.getMembers().stream().anyMatch(
                          identifier -> identifier.getPattern().equals(memberName)
                      )
                  )
              ).forEach(
                  repositoryRule -> repositoryRule.getIdentifiables().forEach(
                      identifiable -> repositories.add(identifiable.getPattern())
                  )
              )
      );
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }

    return repositories;
  }
}
