package cz.janurbanec.sbogbackend.git;

import java.io.File;
import nl.tudelft.ewi.gitolite.git.GitManagerFactory;

public class SbogGitManagerFactory implements GitManagerFactory {

  @Override
  public SbogGitManager create(File file) {
    return new SbogGitManager(file);
  }
}
