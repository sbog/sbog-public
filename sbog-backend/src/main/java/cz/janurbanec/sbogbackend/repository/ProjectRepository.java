package cz.janurbanec.sbogbackend.repository;


import cz.janurbanec.sbogbackend.model.Project;
import cz.janurbanec.sbogbackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {
    Boolean existsByNameAndUser(String name, User user);
    int countByUser(User user);
}
