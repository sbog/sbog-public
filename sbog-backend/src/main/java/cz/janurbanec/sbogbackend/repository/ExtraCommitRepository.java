package cz.janurbanec.sbogbackend.repository;

import cz.janurbanec.sbogbackend.model.ExtraCommit;
import cz.janurbanec.sbogbackend.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExtraCommitRepository extends JpaRepository<ExtraCommit, UUID> {
    ExtraCommit findFirstByNameAndProject(String name, Project project);
    Boolean existsByNameAndProject(String name, Project project);
}
