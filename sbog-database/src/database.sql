create table USERS
(
	ID_USER VARCHAR2(40) not null
		constraint USERS_PK
			primary key,
	USERNAME VARCHAR2(40) not null,
	PASSWORD VARCHAR2(60) not null,
	ROLE NUMBER not null
)
/

create unique index USERS_USERNAME_UINDEX
	on USERS (USERNAME)
/

create table PROJECTS
(
	ID_PROJECT VARCHAR2(40) not null
		constraint PROJECTS_PK
			primary key,
	NAME_PROJECT VARCHAR2(260) not null,
	CREATE_DATE DATE not null,
	ID_USER VARCHAR2(40) not null
		constraint PROJECTS_USERS_ID_USER_FK
			references USERS
				on delete cascade
)
/

create table EXTRA_COMMITS
(
	ID_EXTRA_COMMIT VARCHAR2(40) not null
		constraint EXTRA_COMMIT_PK
			primary key,
	NAME_EXTRA_COMMIT VARCHAR2(40) not null,
	TYPE_EXTRA_COMMIT NUMBER not null,
	ID_PROJECT VARCHAR2(40) not null
		constraint EXTRA_COMMIT_PROJECT_FK
			references PROJECTS
				on delete cascade
)
/

create table COMMENTS
(
	ID_COMMENT VARCHAR2(40) not null
		constraint COMMENTS_PK
			primary key,
	COMMENT_TEXT VARCHAR2(4000) not null,
	CREATE_DATE DATE not null,
	ID_USER VARCHAR2(40) not null
		constraint COMMENTS_USERS_FK
			references USERS
				on delete cascade,
	ID_EXTRA_COMMIT VARCHAR2(40) not null
		constraint COMMENTS_EXTRA_COMMIT_FK
			references EXTRA_COMMITS
				on delete cascade
)
/