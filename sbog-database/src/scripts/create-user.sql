create tablespace sbog_tabspace
datafile 'sbog_tabspace.dat'
size 20M autoextend on;

create TEMPORARY tablespace sbog_tabspace_temp
TEMPFILE 'sbog_tabspace_temp.dat'
size 5M autoextend on;

alter session set "_ORACLE_SCRIPT"=true;
commit;

create user sbog
identified by "&1"
default tablespace sbog_tabspace
temporary tablespace sbog_tabspace_temp;

grant create session to sbog;
grant create table to sbog;
grant unlimited tablespace to sbog;